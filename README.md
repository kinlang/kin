# Kin

☢️ **KIN IS CURRENTLY IN UNSTABLE ALPHA** ☢️

Stack-based programming language which transpiles to ASM

Kin Roadmap:
- [x] Compiled
- [x] Native
- [x] Stack-based
- [ ] Turing Complete
- [ ] Statically Typed
- [ ] Self Hosted

## Quick Start

Powershell (compiling isn't supported):
```powershell
$ py .\kin.py sim .\examples\test.kin
```

Bash:
```shell
$ ./kin.py sim examples/test.kin
$ ./kin.py com examples/test.kin
$ ./output
```